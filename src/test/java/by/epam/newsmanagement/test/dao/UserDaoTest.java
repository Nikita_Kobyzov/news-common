package by.epam.newsmanagement.test.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Locale;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import by.epam.newsmanagement.dao.IUserDao;
import by.epam.newsmanagement.dao.exception.DaoException;
import by.epam.newsmanagement.domain.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/testContext.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:/dataset/userDB.xml")
@DatabaseTearDown(value = "classpath:/dataset/userDB.xml", type = DatabaseOperation.DELETE_ALL)
public class UserDaoTest {
	@Autowired
	private IUserDao userDao;
	@BeforeClass
	public static void setLocale(){
		Locale.setDefault(Locale.ENGLISH);
	}
	@Test
	public void createTest() throws DaoException {
		long countBefore = userDao.read().size();
		User user = new User();
		user.setLogin("test");
		user.setPassword("test");
		user.setUser_name("test");
		userDao.create(user);
		long countAfter = userDao.read().size();
		assertEquals(countBefore + 1, countAfter);
	}

	@Test
	public void readTest() throws DaoException {
		long user_id = 1;
		User user = userDao.read(user_id);
		List<User> userList = userDao.read();
		assertTrue(userList.contains(user));
	}

	@Test
	public void updateTest() throws DaoException {
		long user_id = 1;
		User user = userDao.read(user_id);
		user.setLogin("updated");
		userDao.update(user_id, user);
		List<User> userList = userDao.read();
		assertTrue(userList.contains(user));
	}

	@Test
	public void deleteTest() throws DaoException {
		long user_id = 4;
		User user = userDao.read(user_id);
		userDao.delete(user_id);
		List<User> userList = userDao.read();
		assertFalse(userList.contains(user));
	}

	@Test
	public void readOnLoginTest() throws DaoException {
		User user = new User();
		user.setLogin("test");
		user.setPassword("test");
		user.setUser_name("test");
		long id = userDao.create(user);
		user.setUser_ID(id);
		assertEquals(user, userDao.readOnLogin(user.getLogin()));
	}

}