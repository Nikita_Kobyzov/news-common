package by.epam.newsmanagement.test.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Locale;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import by.epam.newsmanagement.dao.IRoleDao;
import by.epam.newsmanagement.dao.IUserDao;
import by.epam.newsmanagement.dao.exception.DaoException;
import by.epam.newsmanagement.domain.Role;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/testContext.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })

@DatabaseSetup("classpath:/dataset/userDB.xml")
@DatabaseSetup("classpath:/dataset/roleDB.xml")
@DatabaseTearDown(value = "classpath:/dataset/roleDB.xml", type = DatabaseOperation.DELETE_ALL)
@DatabaseTearDown(value = "classpath:/dataset/userDB.xml", type = DatabaseOperation.DELETE_ALL)
public class RoleDaoTest {
	@Autowired
	private IRoleDao roleDao;

	@Autowired
	private IUserDao userDao;
	@BeforeClass
	public static void setLocale(){
		Locale.setDefault(Locale.ENGLISH);
	}
	@Test
	public void createTest() throws DaoException {
		long user_id = 1;
		long countBefore = roleDao.read().size();
		Role role = new Role();
		role.setRole_name("test");
		role.setUser_id(user_id);
		roleDao.create(role);
		long countAfter = roleDao.read().size();
		assertEquals(countBefore + 1, countAfter);
	}

	@Test
	public void readTest() throws DaoException {
		long role_id = 1;
		Role role = roleDao.read(role_id);
		List<Role> roleList = roleDao.read();
		assertTrue(roleList.contains(role));
	}

	@Test
	public void updateTest() throws DaoException {
		long role_id = 1;
		Role role = roleDao.read(role_id);
		role.setRole_name("updated");
		roleDao.update(role_id, role);
		List<Role> roleList = roleDao.read();
		assertTrue(roleList.contains(role));
	}

	@Test
	public void deleteTest() throws DaoException {
		long role_id = 1;
		Role role = roleDao.read(role_id);
		roleDao.delete(role_id);
		List<Role> roleList = roleDao.read();
		assertFalse(roleList.contains(role));
	}

}