package by.epam.newsmanagement.test.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.*;
import java.util.stream.Collectors;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import by.epam.newsmanagement.dao.IAuthorDao;
import by.epam.newsmanagement.dao.INewsDao;
import by.epam.newsmanagement.dao.ITagDao;
import by.epam.newsmanagement.dao.exception.DaoException;
import by.epam.newsmanagement.dao.searchcriteria.SearchCriteria;
import by.epam.newsmanagement.domain.Author;
import by.epam.newsmanagement.domain.News;
import by.epam.newsmanagement.domain.Tag;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/testContext.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class})
@DatabaseSetup("classpath:/dataset/tagDB.xml")
@DatabaseSetup("classpath:/dataset/authorDB.xml")
@DatabaseSetup("classpath:/dataset/newsDB.xml")
@DatabaseSetup("classpath:/dataset/newsAuthorDB.xml")
@DatabaseSetup("classpath:/dataset/newsTagDB.xml")
@DatabaseTearDown(value = "classpath:/dataset/newsAuthorDB.xml", type = DatabaseOperation.DELETE_ALL)
@DatabaseTearDown(value = "classpath:/dataset/newsTagDB.xml", type = DatabaseOperation.DELETE_ALL)
@DatabaseTearDown(value = "classpath:/dataset/tagDB.xml", type = DatabaseOperation.DELETE_ALL)
@DatabaseTearDown(value = "classpath:/dataset/newsDB.xml", type = DatabaseOperation.DELETE_ALL)
@DatabaseTearDown(value = "classpath:/dataset/authorDB.xml", type = DatabaseOperation.DELETE_ALL)
public class NewsDaoTest {
    @Autowired
    private INewsDao newsDao;

    @Autowired
    private ITagDao tagDao;

    @Autowired
    private IAuthorDao authorDao;

    @BeforeClass
    public static void setLocale() {
        Locale.setDefault(Locale.ENGLISH);
    }

    @Test
    public void createTest() throws DaoException {
        long countBefore = newsDao.read().size();
        News news = new News();
        news.setTitle("Test title");
        news.setShort_text("Short test");
        news.setFull_text("Full test");
        Long news_id = newsDao.create(news);
        long countAfter = newsDao.read().size();
        News db_news = newsDao.read(news_id);
        assertEquals(db_news.getTitle(), news.getTitle());
        assertEquals(db_news.getNews_id(), news_id);
        assertEquals(db_news.getShort_text(), db_news.getShort_text());
        assertEquals(db_news.getFull_text(), news.getFull_text());
        assertEquals(countBefore + 1, countAfter);
    }

    @Test
    public void readTest() throws DaoException {
        long news_id = 1;
        News news = newsDao.read(news_id);
        List<News> newsList = newsDao.read();
        assertTrue(newsList.contains(news));
    }

    @Test
    public void updateTest() throws DaoException {
        long news_id = 1;
        News news = newsDao.read(news_id);
        news.setTitle("Test title");
        newsDao.update(news_id, news);
        News db_news = newsDao.read(news_id);
        assertEquals(db_news.getTitle(), news.getTitle());
        List<News> newsList = newsDao.read();
        assertTrue(newsList.contains(db_news));
    }

    @Test
    public void deleteTest() throws DaoException {
        News news = new News();
        news.setTitle("Test title");
        news.setShort_text("Short test");
        news.setFull_text("Full test");
        news.setCreation_date(new Date());
        news.setModification_date(new Date());
        long id = newsDao.create(news);
        news.setNews_id(id);
        newsDao.delete(id);
        List<News> newsList = newsDao.read();
        assertFalse(newsList.contains(news));
    }

    @Test
    public void readOnAuthorCriteriaTest() throws DaoException {
        Long news_id = 1L;
        Author author = authorDao.readOnNewsId(news_id);
        List<Long> authorList = new ArrayList<Long>();
        authorList.add(author.getAuthor_id());
        SearchCriteria sc = new SearchCriteria(authorList, Collections.EMPTY_LIST);
        List<News> newsList = newsDao.readByCriteria(1L, 100, sc);
        assertEquals(newsList.size(), 1);
        assertEquals(newsList.get(0).getNews_id(), news_id);
    }

    @Test
    public void readOnTagCriteriaTest() throws DaoException {
        Long news_id = 1L;
        List<Long> tagList = tagDao.readOnNewsId(news_id).stream()
                .map(Tag::getTag_id).collect(Collectors.toList());
        SearchCriteria sc = new SearchCriteria(Collections.EMPTY_LIST, tagList);
        List<News> newsList = newsDao.readByCriteria(1L, 10, sc);
        assertEquals(newsList.size(), 1);
        assertEquals(newsList.get(0).getNews_id(), news_id);
    }

    @Test
    public void readOnCriteriaTest() throws DaoException {
        Long news_id = 1l;
        List<Long> tagList = tagDao.readOnNewsId(news_id).stream()
                .map(Tag::getTag_id).collect(Collectors.toList());
        Author author = authorDao.readOnNewsId(news_id);
        List<Long> authorList = new ArrayList<Long>();
        authorList.add(author.getAuthor_id());
        SearchCriteria sc = new SearchCriteria(authorList, tagList);
        List<News> newsList = newsDao.readByCriteria(1L, 10, sc);
        assertEquals(newsList.size(), 1);
        assertEquals(newsList.get(0).getNews_id(), news_id);
    }

    @Test
    public void newsTagTest() throws DaoException {
        long news_id = 1;
        Tag tag = new Tag();
        tag.setTag_name("test");
        long tag_id = tagDao.create(tag);
        tag.setTag_id(tag_id);
        newsDao.connectNewsWithTag(news_id, tag_id);
        List<Tag> tagList = tagDao.readOnNewsId(news_id);
        assertTrue(tagList.contains(tag));
        newsDao.deleteNewsTag(news_id);
        tagList = tagDao.readOnNewsId(news_id);
        assertFalse(tagList.contains(tag));
    }

    @Test
    public void newsAuthorTest() throws DaoException {
        long news_id = 6;
        Author author = new Author();
        author.setAuthor_name("test");
        long author_id = authorDao.create(author);
        author.setAuthor_id(author_id);
        newsDao.connectNewsWithAuthor(news_id, author_id);
        Author dbAuthor = authorDao.readOnNewsId(news_id);
        assertEquals(dbAuthor, author);
        newsDao.deleteNewsAuthor(news_id);
        dbAuthor = authorDao.readOnNewsId(news_id);
        assertNull(dbAuthor);
    }

}