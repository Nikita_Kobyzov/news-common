package by.epam.newsmanagement.test.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import by.epam.newsmanagement.dao.IAuthorDao;
import by.epam.newsmanagement.dao.INewsDao;
import by.epam.newsmanagement.dao.exception.DaoException;
import by.epam.newsmanagement.domain.Author;
import by.epam.newsmanagement.domain.News;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/testContext.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@DatabaseSetup("/dataset/authorDB.xml")
@DatabaseSetup("/dataset/newsDB.xml")
@DatabaseSetup("/dataset/newsAuthorDB.xml")
@DatabaseTearDown(value = "/dataset/newsAuthorDB.xml", type = DatabaseOperation.DELETE_ALL)
@DatabaseTearDown(value = "/dataset/newsDB.xml", type = DatabaseOperation.DELETE_ALL)
@DatabaseTearDown(value = "/dataset/authorDB.xml", type = DatabaseOperation.DELETE_ALL)
public class AuthorDaoTest {
	@Autowired
	private IAuthorDao authorDao;
	@Autowired
	private INewsDao newsDao;

	@BeforeClass
	public static void setLocale(){
		Locale.setDefault(Locale.ENGLISH);
	}

	@Test
	public void createTest() throws DaoException {
		long countBefore = authorDao.read().size();
		Author author = new Author();
		author.setAuthor_name("Test");
		Long auth_id = authorDao.create(author);
		long countAfter = authorDao.read().size();
		Author db_author = authorDao.read(auth_id);
		assertEquals(author.getAuthor_name(), db_author.getAuthor_name());
		assertEquals(db_author.getAuthor_id(), auth_id);
		assertEquals(countBefore + 1, countAfter);
	}

	@Test
	public void readTest() throws DaoException {
		long author_id = 1;
		Author author = authorDao.read(author_id);
		List<Author> authorList = authorDao.read();
		assertTrue(authorList.contains(author));
	}

	@Test
	public void updateTest() throws DaoException {
		long author_id = 1;
		Author updAuthor = authorDao.read(author_id);
		updAuthor.setAuthor_name("updated");
		authorDao.update(author_id, updAuthor);
		List<Author> authorList = authorDao.read();
		assertTrue(authorList.contains(updAuthor));
	}

	@Test
	public void deleteTest() throws DaoException {
		long author_id = 5;
		Author author = authorDao.read(author_id);
		authorDao.delete(author_id);
		List<Author> authorList = authorDao.read();
		assertFalse(authorList.contains(author));
	}

	@Test
	public void readOnNewsIdTest() throws DaoException {
		Author author = new Author();
		author.setAuthor_name("Test");
		long id = authorDao.create(author);
		author.setAuthor_id(id);
		News news = new News();
		news.setTitle("Test title");
		news.setShort_text("Short test");
		news.setFull_text("Full test");
		news.setCreation_date(new Date());
		news.setModification_date(new Date());
		long news_id = newsDao.create(news);
		newsDao.connectNewsWithAuthor(news_id, id);
		Author foundAuthor = authorDao.readOnNewsId(news_id);
		assertEquals(foundAuthor, author);
	}

	@Test
	public void makeExpiredTest() throws DaoException {
		long author_id = 1;
		Author author = authorDao.read(author_id);
		assertNull(author.getExpired());
		authorDao.makeExpired(author_id);
		author = authorDao.read(author_id);
		assertNotNull(author.getExpired());
	}

}