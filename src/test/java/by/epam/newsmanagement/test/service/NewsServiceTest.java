package by.epam.newsmanagement.test.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import by.epam.newsmanagement.dao.searchcriteria.SearchCriteria;
import by.epam.newsmanagement.service.impl.NewsManagementServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import by.epam.newsmanagement.dao.IAuthorDao;
import by.epam.newsmanagement.dao.ICommentDao;
import by.epam.newsmanagement.dao.INewsDao;
import by.epam.newsmanagement.dao.ITagDao;
import by.epam.newsmanagement.dao.exception.DaoException;
import by.epam.newsmanagement.domain.Author;
import by.epam.newsmanagement.domain.News;
import by.epam.newsmanagement.domain.Tag;
import by.epam.newsmanagement.domain.dto.NewsDTO;
import by.epam.newsmanagement.service.exception.ServiceException;
import by.epam.newsmanagement.service.impl.NewsServiceImpl;

public class NewsServiceTest {

	@Mock
	private INewsDao newsDao;


	@InjectMocks
	private NewsManagementServiceImpl newsManagementService;

	@InjectMocks
	private NewsServiceImpl newsService;



	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getNewsBySearchCriteriaTest_ShouldReturnNewsList() throws ServiceException, DaoException{
		List<News> newsList = Arrays.asList(new News(1L) , new News(2L));
		SearchCriteria criteria = new SearchCriteria(Arrays.asList(4L, 5L), Arrays.asList(6L, 7L));
		long page = 3L;
		when(newsService.getNewsBySearchCriteria(page, criteria)).thenReturn(newsList);
		Assert.assertEquals(newsList, newsService.getNewsBySearchCriteria(page, criteria));
	}
	@Test
	public void addNewsTest_ShouldCreateNewsWithEmptyTagList() throws ServiceException, DaoException {
		Long news_id = new Long(1);
		Long author_id = new Long(2);
		News news = new News();
		Author author = new Author();
		author.setAuthor_id(author_id);
		NewsDTO newsTO = new NewsDTO();
		newsTO.setTagList(Collections.EMPTY_LIST);
		newsTO.setAuthor(author);
		newsTO.setNews(news);
		when(newsDao.create(news)).thenReturn(news_id);
		assertTrue(newsService.addNews(newsTO));
		verify(newsDao).create(news);
		verify(newsDao).connectNewsWithAuthor(news_id, author_id);
	}

	@Test
	public void deleteNewsTest_ShouldDeleteNews() throws DaoException, ServiceException {
		long news_id = 1L;
		newsService.deleteNews(news_id);
		verify(newsDao).deleteNewsAuthor(news_id);
		verify(newsDao).deleteNewsTag(news_id);
		verify(newsDao).delete(news_id);
	}

	@Test(expected = ServiceException.class)
	public void deleteNewsTest_ShouldThrowException() throws DaoException, ServiceException {
		long news_id = 1L;
		when(newsDao.deleteNewsTag(news_id)).thenThrow(DaoException.class);
		newsService.deleteNews(news_id);
	}

	@Test
	public void addNewsTest_ShouldCreateNewsWithFullTagList() throws ServiceException, DaoException {
		Long news_id = new Long(1);
		Long author_id = new Long(2);
		News news = new News(news_id);
		Author author = new Author(author_id);
		author.setAuthor_id(author_id);
		List<Tag> tagList = Arrays.asList(new Tag(3L) , new Tag(4L), new Tag(5L));
		NewsDTO newsTO = new NewsDTO();
		newsTO.setTagList(tagList);
		newsTO.setAuthor(author);
		newsTO.setNews(news);
		when(newsDao.create(news)).thenReturn(news_id);
		assertTrue(newsService.addNews(newsTO));
		verify(newsDao).create(news);
		verify(newsDao).connectNewsWithAuthor(news_id, author_id);
		verify(newsDao).connectNewsWithTag(news_id, 3L);
		verify(newsDao).connectNewsWithTag(news_id, 4L);
		verify(newsDao).connectNewsWithTag(news_id, 5L);

	}

	@Test(expected = ServiceException.class)
	public void addNewsTest_ShouldThrowException() throws ServiceException, DaoException {
		News news = new News();
		NewsDTO newsTO = new NewsDTO();
		newsTO.setNews(news);
		when(newsDao.create(news)).thenThrow(DaoException.class);
		newsService.addNews(newsTO);
	}


	@Test
	public void editNewsTagsConnectionTest() throws ServiceException, DaoException{
		Long news_id = 1L;
		List<Long> new_tag_id_list = Arrays.asList(4L, 5L, 6L, 7L);
		newsService.editNewsTagsConnection(news_id, new_tag_id_list);
		verify(newsDao).deleteNewsTag(news_id);
		verify(newsDao).connectNewsWithTag(news_id, 6L);
		verify(newsDao).connectNewsWithTag(news_id, 7L);
	}

}
