package by.epam.newsmanagement.service;

import java.util.List;

import by.epam.newsmanagement.domain.Author;
import by.epam.newsmanagement.service.exception.ServiceException;

/**
 * <p>
 * This interface is designed to solve problems of business logic for a
 * Author-entity
 * </p>
 *
 * @author Mikita_Kobyzau
 */
public interface IAuthorService {

    /**
     * <p>
     * This method writes a new author
     * </p>
     *
     * @param author is the author that to be recorded
     * @throws ServiceException if an error has occurred with the write operations
     */
    void addAuthor(Author author) throws ServiceException;

    /**
     * <p>
     * This method makes author expired. In field "expired" sets the current
     * date
     * </p>
     *
     * @param author_id is a unique identifier of the author that to be expired
     * @throws ServiceException if an error has occurred with the search operations
     */
    void makeExpired(long author_id) throws ServiceException;

    /**
     * <p>
     * This method reads all authors
     * </p>
     *
     * @return a list of all found authors
     * @throws ServiceException if an error has occurred with the search operations
     */
    List<Author> getAllAuthors() throws ServiceException;

    /**
     * <p>
     * This method update an author on a given author id
     * </p>
     *
     * @param author_id is a unique identifier of the author that to be changed
     * @param author    is a author that to be recorded
     * @throws ServiceException if an error has occurred with the update operations
     */
    void editAuthor(long author_id, Author author) throws ServiceException;

    /**
     * @param news_id
     * @return
     * @throws ServiceException
     */
    Author readAuthorByNewsId(Long news_id) throws ServiceException;
}
