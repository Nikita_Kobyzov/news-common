package by.epam.newsmanagement.service;

import by.epam.newsmanagement.domain.User;
import by.epam.newsmanagement.service.exception.ServiceException;

/**
 * <p>
 * This interface is designed to solve problems of business logic for a
 * User-entity
 * </p>
 *
 * @author Mikita_Kobyzau
 */
public interface IUserService {

    /**
     * <p>
     * This method add a new user if there isn't user with same login
     * <p>
     *
     * @param user is the user that to be recorded
     * @return the user, if he was recorded or <tt>null</tt> otherwise
     * @throws ServiceException if an error has occurred with the write operations
     */
    User registration(User user) throws ServiceException;

    /**
     * <p>
     * This method is used for user authorization
     * </p>
     *
     * @param login    is the login of the user
     * @param password is the password of the user
     * @return the user, if he was authorized or <tt>null</tt> otherwise
     * @throws ServiceException if an error has occurred with the write operations
     */
    User login(String login, String password) throws ServiceException;
}
