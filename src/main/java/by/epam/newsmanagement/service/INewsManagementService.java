package by.epam.newsmanagement.service;

import by.epam.newsmanagement.dao.searchcriteria.SearchCriteria;
import by.epam.newsmanagement.domain.News;
import by.epam.newsmanagement.domain.dto.NewsDTO;
import by.epam.newsmanagement.service.exception.ServiceException;

import java.util.List;

/**
 * Created by NikitOS on 06.03.2016.
 */
public interface INewsManagementService {
    /**
     * <p>
     * This method deletes news on news id
     * </p>
     *
     * @param news_id is a unique identifier of the news
     * @throws ServiceException if an error has occurred with the write operations
     */
    void deleteNews(long news_id) throws ServiceException;

    /**
     * <p>
     * This method reads the news with author and tags on news id
     * </p>
     *
     * @param news_id is a unique identifier of the news
     * @return the transfer object of news with author and tags
     * @throws ServiceException if an error has occurred with the write operations
     */
    NewsDTO getNewsTO(long news_id) throws ServiceException;

    /**
     * <p>
     * This method reads all news with authors and tags corresponding to the
     * given search criteria
     * </p>
     *
     * @param page           is a number of page
     * @param searchCriteria is an object that contains the criteria for the search
     * @return
     * @throws ServiceException if an error has occurred with the write operations
     */
    List<NewsDTO> getNewsBySearchCriteria(Long page, SearchCriteria searchCriteria) throws ServiceException;


    /**
     * <p>
     * This method edits  news with authors and tags corresponding to the
     * given news id
     * </p>
     * @param news_id is a unique identifier of the news
     * @param news is a news that to be recorded
     * @param tag_id_list is an updated list of tags of the news
     * @param author_id is an updated author of the news
     * @throws ServiceException if an error has occurred with the write operations
     */
    void editNews(long news_id, News news, List<Long> tag_id_list, Long author_id) throws ServiceException;
}
