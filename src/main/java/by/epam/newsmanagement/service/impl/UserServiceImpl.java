package by.epam.newsmanagement.service.impl;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.epam.newsmanagement.dao.IUserDao;
import by.epam.newsmanagement.dao.exception.DaoException;
import by.epam.newsmanagement.domain.User;
import by.epam.newsmanagement.service.IUserService;
import by.epam.newsmanagement.service.exception.ServiceException;

/**
 * <p>
 * This class is designed to solve problems of business logic for a
 * User-entity
 * </p>
 *
 * @author Mikita_Kobyzau
 */
@Service("userService")
public class UserServiceImpl implements IUserService {

    private static final Lock REGISTRATION_LOCK = new ReentrantLock();

    @Autowired
    private IUserDao userDao;

    public List<User> getUsers() throws ServiceException {
        try {
            List<User> result = userDao.read();
            return result;
        } catch (DaoException e) {
            throw new ServiceException("Exception in User Service", e);
        }
    }

    public User registration(User user) throws ServiceException {
        try {
            REGISTRATION_LOCK.lock();
            User dsUser = userDao.readOnLogin(user.getLogin());
            if (dsUser != null) {
                return null;
            }
            long user_id = userDao.create(user);
            user.setUser_ID(user_id);
        } catch (DaoException e) {
            throw new ServiceException("Exception in User Service with user = " + user, e);
        } finally {
            REGISTRATION_LOCK.unlock();
        }
        return user;
    }

    public User login(String login, String password) throws ServiceException {
        try {
            User dsUser = userDao.readOnLogin(login);
            if (dsUser == null) {
                return null;
            }
            if (dsUser.getPassword().equals(password)) {
                return dsUser;
            } else {
                return null;
            }
        } catch (DaoException e) {
            throw new ServiceException("Exception in User Service with login = "
                    + login + ", password = " + password, e);
        }

    }
}
