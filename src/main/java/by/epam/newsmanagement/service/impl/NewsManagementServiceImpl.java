package by.epam.newsmanagement.service.impl;

import by.epam.newsmanagement.dao.searchcriteria.SearchCriteria;
import by.epam.newsmanagement.domain.Author;
import by.epam.newsmanagement.domain.Comment;
import by.epam.newsmanagement.domain.News;
import by.epam.newsmanagement.domain.Tag;
import by.epam.newsmanagement.domain.dto.NewsDTO;
import by.epam.newsmanagement.service.*;
import by.epam.newsmanagement.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by NikitOS on 07.03.2016.
 */
public class NewsManagementServiceImpl implements INewsManagementService {

    @Autowired
    private INewsService newsService;
    @Autowired
    private IAuthorService authorService;
    @Autowired
    private ITagService tagService;
    @Autowired
    private ICommentService commentService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteNews(long news_id) throws ServiceException {
        commentService.deleteCommentOnNewsId(news_id);
        newsService.deleteNews(news_id);
    }

    @Override
    public NewsDTO getNewsTO(long news_id) throws ServiceException {
        NewsDTO newsDTO = new NewsDTO();
        News news = newsService.getNews(news_id);
        newsDTO.setNews(news);
        Author author = authorService.readAuthorByNewsId(news_id);
        newsDTO.setAuthor(author);
        List<Tag> tagList = tagService.readByNewsId(news_id);
        newsDTO.setTagList(tagList);
        List<Comment> commentList = commentService.readByNewsId(news_id);
        newsDTO.setCommentList(commentList);
        return newsDTO;
    }

    @Override
    public List<NewsDTO> getNewsBySearchCriteria(Long page, SearchCriteria searchCriteria) throws ServiceException {
        List<News> newsList = newsService.getNewsBySearchCriteria(page, searchCriteria);
        List<NewsDTO> newsDTOList = new ArrayList<NewsDTO>();
        for (News news : newsList) {
            NewsDTO newsDTO = new NewsDTO();
            newsDTO.setNews(news);
            Author author = authorService.readAuthorByNewsId(news.getNews_id());
            newsDTO.setAuthor(author);
            List<Tag> tagList = tagService.readByNewsId(news.getNews_id());
            newsDTO.setTagList(tagList);
            List<Comment> commentList = commentService.readByNewsId(news.getNews_id());
            newsDTO.setCommentList(commentList);
            newsDTOList.add(newsDTO);
        }
        return newsDTOList;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void editNews(long news_id, News news, List<Long> tag_id_list, Long author_id) throws ServiceException {
        newsService.editNews(news_id, news);
        Author author = authorService.readAuthorByNewsId(news_id);
        if (!author.getAuthor_id().equals(author_id)) {
            newsService.editNewsAuthorConnection(news_id, author_id);
        }
        newsService.editNewsTagsConnection(news_id,  tag_id_list);
    }
}
