package by.epam.newsmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.epam.newsmanagement.dao.ITagDao;
import by.epam.newsmanagement.dao.exception.DaoException;
import by.epam.newsmanagement.domain.Tag;
import by.epam.newsmanagement.service.ITagService;
import by.epam.newsmanagement.service.exception.ServiceException;

/**
 * <p>
 * This class is designed to solve problems of business logic for a Tag-entity
 * </p>
 *
 * @author Mikita_Kobyzau
 */
@Service("tagService")
public class TagServiceImpl implements ITagService {

    @Autowired
    private ITagDao tagDao;

    public boolean addTag(Tag tag) throws ServiceException {
        try {
            List<Tag> tagList = tagDao.read();
            for (Tag dsTag : tagList) {
                if (dsTag.getTag_name().equals(tag.getTag_name())) {
                    return false;
                }
            }
            tagDao.create(tag);
        } catch (DaoException e) {
            throw new ServiceException("Exception in Tag Service with tag = " + tag, e);
        }
        return true;
    }

    @Override
    public List<Tag> readByNewsId(Long news_id) throws ServiceException {
        try {
            List<Tag> tagList = tagDao.readOnNewsId(news_id);
            return tagList;
        } catch (DaoException e) {
            throw new ServiceException("Exception in Tag Service with news_id" + news_id, e);
        }
    }

    public List<Tag> readAll() throws ServiceException {
        try {
            List<Tag> tagList =tagDao.read();
            return tagList;
        } catch (DaoException e) {
            throw new ServiceException("Exception in Tag Service", e);
        }
    }

    @Override
    public void deleteTag(long tag_id) throws ServiceException {
        try {
            tagDao.delete(tag_id);
        } catch (DaoException e) {
            throw new ServiceException("Exception in Tag Service with tag_id = " + tag_id, e);
        }
    }

    @Override
    public void updateTag(long tag_id, Tag tag) throws ServiceException {
        try {
            tagDao.update(tag_id, tag);
        } catch (DaoException e) {
            throw new ServiceException("Exception in Tag Service with tag_id = "
                    + tag_id + ", tag = " + tag, e);
        }
    }
}
