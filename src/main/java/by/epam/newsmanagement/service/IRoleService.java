package by.epam.newsmanagement.service;

import by.epam.newsmanagement.domain.Role;
import by.epam.newsmanagement.service.exception.ServiceException;

/**
 * <p>
 * This interface is designed to solve problems of business logic for a Role-entity
 * </p>
 *
 * @author Mikita_Kobyzau
 */
public interface IRoleService {

    /**
     * <p>
     * This method writes a new role
     * </p>
     *
     * @param role is the role that to be recorded
     * @throws ServiceException if an error has occurred with the write operations
     */
    void addRole(Role role) throws ServiceException;

}
