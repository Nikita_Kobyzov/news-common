package by.epam.newsmanagement.service;

import java.util.List;

import by.epam.newsmanagement.dao.searchcriteria.SearchCriteria;
import by.epam.newsmanagement.domain.News;
import by.epam.newsmanagement.domain.dto.NewsDTO;
import by.epam.newsmanagement.service.exception.ServiceException;

/**
 * <p>
 * This interface is designed to solve problems of business logic for a
 * News-entity
 * </p>
 *
 * @author Mikita_Kobyzau
 */
public interface INewsService {

    /**
     * <p>
     * This method adds a new news
     * </p>
     *
     * @param newsDTO is the transfer object of news with author and tags that to be
     *                recorded
     * @return <tt>true</tt> if the operation was successful
     * @throws ServiceException if an error has occurred with the write operations
     */
    boolean addNews(NewsDTO newsDTO) throws ServiceException;

    /**
     * <p>
     * This method reads the news with author and tags on news id
     * </p>
     *
     * @param news_id is a unique identifier of the news
     * @return the news corresponding to the given news id
     * @throws ServiceException if an error has occurred with the write operations
     */
    News getNews(long news_id) throws ServiceException;

    /**
     * <p>
     * This method reads  news with authors and tags corresponding to the
     * given search criteria
     * </p>
     *
     * @param page           is a number of page
     * @param searchCriteria is an object that contains the criteria for the search
     * @return the news corresponding to the given search criteria
     * @throws ServiceException if an error has occurred with the write operations
     */
    List<News> getNewsBySearchCriteria(Long page, SearchCriteria searchCriteria) throws ServiceException;

    /**
     * <p>
     * This method deletes news on news id
     * </p>
     *
     * @param news_id is a unique identifier of the news
     * @throws ServiceException if an error has occurred with the write operations
     */
    void deleteNews(long news_id) throws ServiceException;

    /**
     * <p>
     * This method edits news with author and tags on news id
     * </p>
     *
     * @param news_id is a unique identifier of the news
     * @param news    is the  news that to be recorded
     * @throws ServiceException if an error has occurred with the write operations
     */
    void editNews(long news_id, News news) throws ServiceException;

    /**
     * <p>
     * This method sets a new relation between news and author
     * </p>
     *
     * @param news_id       is a unique identifier of the news
     * @param new_author_id is a unique identifier of the author that to be recorded
     * @throws ServiceException if an error has occurred with the write operations
     */
    void editNewsAuthorConnection(Long news_id, Long new_author_id) throws ServiceException;

    /**
     * <p>
     * This method sets a new relation between news and tags
     * </p>
     *
     * @param news_id         is a unique identifier of the news
     * @param new_tag_id_list is a list of  unique identifier of the tags that to be recorded
     * @throws ServiceException
     */
    void editNewsTagsConnection(Long news_id, List<Long> new_tag_id_list) throws ServiceException;
}
