package by.epam.newsmanagement.domain.dto;

import java.io.Serializable;
import java.util.List;

import by.epam.newsmanagement.domain.Role;
import by.epam.newsmanagement.domain.User;

/**
 * <p>
 * This class describes the data transfer object User. This class consist of
 * user and list of roles
 * </p>
 * 
 * @author NikitOS
 *
 */
public class UserDTO implements Serializable{

	private User user;
	private List<Role> roleList;

	public UserDTO() {
	}

	public UserDTO(User user, List<Role> roleList) {
		this.user = user;
		this.roleList = roleList;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Role> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<Role> roleList) {
		this.roleList = roleList;
	}

	@Override
	public String toString() {
		return "UserDTO [user=" + user + ", roleList=" + roleList + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((roleList == null) ? 0 : roleList.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserDTO other = (UserDTO) obj;
		if (roleList == null) {
			if (other.roleList != null)
				return false;
		} else if (!roleList.equals(other.roleList))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

}
