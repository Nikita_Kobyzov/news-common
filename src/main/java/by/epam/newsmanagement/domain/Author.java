package by.epam.newsmanagement.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * This class describes the Author-entity
 * </p>
 * 
 * @author Mikita_Kobyzau
 */
public class Author implements Serializable{

	private Long author_id;
	private String author_name;
	private Date expired;

	public Author() {
	}

	public Author(Long author_id, String author_name, Date expired) {
		this.author_id = author_id;
		this.author_name = author_name;
		this.expired = expired;
	}

	public Author(Long author_id) {
		this.author_id = author_id;
	}

	public Long getAuthor_id() {
		return author_id;
	}

	public void setAuthor_id(Long author_id) {
		this.author_id = author_id;
	}

	public String getAuthor_name() {
		return author_name;
	}

	public void setAuthor_name(String author_name) {
		this.author_name = author_name;
	}

	public Date getExpired() {
		return expired;
	}

	public void setExpired(Date expired) {
		this.expired = expired;
	}

	@Override
	public String toString() {
		return "Author [author_id=" + author_id + ", author_name=" + author_name + ", expired=" + expired + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Author author = (Author) o;

		if (author_id != null ? !author_id.equals(author.author_id) : author.author_id != null) return false;
		if (author_name != null ? !author_name.equals(author.author_name) : author.author_name != null) return false;
		return !(expired != null ? !expired.equals(author.expired) : author.expired != null);

	}

	@Override
	public int hashCode() {
		int result = author_id != null ? author_id.hashCode() : 0;
		result = 31 * result + (author_name != null ? author_name.hashCode() : 0);
		result = 31 * result + (expired != null ? expired.hashCode() : 0);
		return result;
	}
}
