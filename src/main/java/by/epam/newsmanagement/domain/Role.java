package by.epam.newsmanagement.domain;

import java.io.Serializable;

/**
 * <p>
 * This class describes the Role-entity
 * </p>
 * 
 * @author Mikita_Kobyzau
 */
public class Role implements Serializable{

	private Long role_id;
	private Long user_id;
	private String role_name;

	public Role() {
	}

	public Role(Long role_id, Long user_id, String role_name) {
		this.role_id = role_id;
		this.user_id = user_id;
		this.role_name = role_name;
	}

	public Role(Long role_id) {
		this.role_id = role_id;
	}

	public Long getRole_id() {
		return role_id;
	}

	public void setRole_id(Long role_id) {
		this.role_id = role_id;
	}

	public Long getUser_id() {
		return user_id;
	}

	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}

	public String getRole_name() {
		return role_name;
	}

	public void setRole_name(String role_name) {
		this.role_name = role_name;
	}

	@Override
	public String toString() {
		return "Role [role_id=" + role_id + ", user_id=" + user_id + ", role_name=" + role_name + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Role role = (Role) o;

		if (role_id != null ? !role_id.equals(role.role_id) : role.role_id != null) return false;
		if (user_id != null ? !user_id.equals(role.user_id) : role.user_id != null) return false;
		return role_name != null ? role_name.equals(role.role_name) : role.role_name == null;

	}

	@Override
	public int hashCode() {
		int result = role_id != null ? role_id.hashCode() : 0;
		result = 31 * result + (user_id != null ? user_id.hashCode() : 0);
		result = 31 * result + (role_name != null ? role_name.hashCode() : 0);
		return result;
	}
}
