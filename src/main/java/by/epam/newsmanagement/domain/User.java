package by.epam.newsmanagement.domain;

import java.io.Serializable;

/**
 * <p>
 * This class describes the User-entity
 * </p>
 * 
 * @author Mikita_Kobyzau
 */
public class User implements Serializable{
	private Long user_ID;
	private String user_name;
	private String login;
	private String password;

	public User() {
	}

	public User(Long user_ID, String user_name, String login, String password) {
		this.user_ID = user_ID;
		this.user_name = user_name;
		this.login = login;
		this.password = password;
	}

	public User(Long user_ID) {
		this.user_ID = user_ID;
	}

	public Long getUser_ID() {
		return user_ID;
	}

	public void setUser_ID(Long user_ID) {
		this.user_ID = user_ID;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "User [user_ID=" + user_ID + ", user_name=" + user_name + ", login=" + login + ", password=" + password
				+ "]";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		User user = (User) o;

		if (user_ID != null ? !user_ID.equals(user.user_ID) : user.user_ID != null) return false;
		if (user_name != null ? !user_name.equals(user.user_name) : user.user_name != null) return false;
		if (login != null ? !login.equals(user.login) : user.login != null) return false;
		return password != null ? password.equals(user.password) : user.password == null;

	}

	@Override
	public int hashCode() {
		int result = user_ID != null ? user_ID.hashCode() : 0;
		result = 31 * result + (user_name != null ? user_name.hashCode() : 0);
		result = 31 * result + (login != null ? login.hashCode() : 0);
		result = 31 * result + (password != null ? password.hashCode() : 0);
		return result;
	}
}
