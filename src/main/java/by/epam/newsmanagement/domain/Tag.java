package by.epam.newsmanagement.domain;

import java.io.Serializable;

/**
 * <p>
 * This class describes the Tag-entity
 * </p>
 * 
 * @author Mikita_Kobyzau
 */
public class Tag implements Serializable{

	private Long tag_id;
	private String tag_name;

	public Tag() {
	}

	public Tag(Long tag_id, String tag_name) {
		this.tag_id = tag_id;
		this.tag_name = tag_name;
	}

	public Tag(Long tag_id) {
		this.tag_id = tag_id;
	}

	public Long getTag_id() {
		return tag_id;
	}

	public void setTag_id(Long tag_id) {
		this.tag_id = tag_id;
	}

	public String getTag_name() {
		return tag_name;
	}

	public void setTag_name(String tag_name) {
		this.tag_name = tag_name;
	}

	@Override
	public String toString() {
		return "Tag [tag_id=" + tag_id + ", tag_name=" + tag_name + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Tag tag = (Tag) o;

		if (tag_id != null ? !tag_id.equals(tag.tag_id) : tag.tag_id != null) return false;
		return tag_name != null ? tag_name.equals(tag.tag_name) : tag.tag_name == null;

	}

	@Override
	public int hashCode() {
		int result = tag_id != null ? tag_id.hashCode() : 0;
		result = 31 * result + (tag_name != null ? tag_name.hashCode() : 0);
		return result;
	}
}
