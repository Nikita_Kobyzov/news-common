package by.epam.newsmanagement.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * This class describes the Comment-entity
 * </p>
 * 
 * @author Mikita_Kobyzau
 */
public class Comment implements Serializable {

	private Long comment_id;
	private Long news_id;
	private String comment_text;
	private Date creation_date;

	public Comment() {
	}

	public Comment(Long comment_id, Long news_id, String comment_text, Date creation_date) {
		this.comment_id = comment_id;
		this.news_id = news_id;
		this.comment_text = comment_text;
		this.creation_date = creation_date;
	}

	public Comment(Long comment_id) {
		this.comment_id = comment_id;
	}

	public Long getComment_id() {
		return comment_id;
	}

	public void setComment_id(Long comment_id) {
		this.comment_id = comment_id;
	}

	public Long getNews_id() {
		return news_id;
	}

	public void setNews_id(Long news_id) {
		this.news_id = news_id;
	}

	public String getComment_text() {
		return comment_text;
	}

	public void setComment_text(String comment_text) {
		this.comment_text = comment_text;
	}

	public Date getCreation_date() {
		return creation_date;
	}

	public void setCreation_date(Date creation_date) {
		this.creation_date = creation_date;
	}

	@Override
	public String toString() {
		return "Comment [comment_id=" + comment_id + ", news_id=" + news_id + ", comment_text=" + comment_text
				+ ", creation_date=" + creation_date + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Comment comment = (Comment) o;

		if (comment_id != null ? !comment_id.equals(comment.comment_id) : comment.comment_id != null) return false;
		if (news_id != null ? !news_id.equals(comment.news_id) : comment.news_id != null) return false;
		if (comment_text != null ? !comment_text.equals(comment.comment_text) : comment.comment_text != null)
			return false;
		return creation_date != null ? creation_date.equals(comment.creation_date) : comment.creation_date == null;

	}

	@Override
	public int hashCode() {
		int result = comment_id != null ? comment_id.hashCode() : 0;
		result = 31 * result + (news_id != null ? news_id.hashCode() : 0);
		result = 31 * result + (comment_text != null ? comment_text.hashCode() : 0);
		result = 31 * result + (creation_date != null ? creation_date.hashCode() : 0);
		return result;
	}
}
