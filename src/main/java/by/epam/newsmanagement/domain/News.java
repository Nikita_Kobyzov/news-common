package by.epam.newsmanagement.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * This class describes the News-entity
 * </p>
 * 
 * @author Mikita_Kobyzau
 */
public class News implements Serializable{

	private Long news_id;
	private String title;
	private String short_text;
	private String full_text;
	private Date creation_date;
	private Date modification_date;

	public News() {
	}

	public News(Long news_id, String title, String short_text, String full_text, Date creation_date,
			Date modification_date) {
		super();
		this.news_id = news_id;
		this.title = title;
		this.short_text = short_text;
		this.full_text = full_text;
		this.creation_date = creation_date;
		this.modification_date = modification_date;
	}

	public News(Long news_id) {
		this.news_id = news_id;
	}

	public Long getNews_id() {
		return news_id;
	}

	public void setNews_id(Long news_id) {
		this.news_id = news_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShort_text() {
		return short_text;
	}

	public void setShort_text(String short_text) {
		this.short_text = short_text;
	}

	public String getFull_text() {
		return full_text;
	}

	public void setFull_text(String full_text) {
		this.full_text = full_text;
	}

	public Date getCreation_date() {
		return creation_date;
	}

	public void setCreation_date(Date creation_date) {
		this.creation_date = creation_date;
	}

	public Date getModification_date() {
		return modification_date;
	}

	public void setModification_date(Date modification_date) {
		this.modification_date = modification_date;
	}

	@Override
	public String toString() {
		return "News [news_id=" + news_id + ", title=" + title + ", short_text=" + short_text + ", full_text="
				+ full_text + ", creation_date=" + creation_date + ", modification_date=" + modification_date + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		News news = (News) o;

		if (news_id != null ? !news_id.equals(news.news_id) : news.news_id != null) return false;
		if (title != null ? !title.equals(news.title) : news.title != null) return false;
		if (short_text != null ? !short_text.equals(news.short_text) : news.short_text != null) return false;
		if (full_text != null ? !full_text.equals(news.full_text) : news.full_text != null) return false;
		if (creation_date != null ? !creation_date.equals(news.creation_date) : news.creation_date != null)
			return false;
		return modification_date != null ? modification_date.equals(news.modification_date) : news.modification_date == null;

	}

	@Override
	public int hashCode() {
		int result = news_id != null ? news_id.hashCode() : 0;
		result = 31 * result + (title != null ? title.hashCode() : 0);
		result = 31 * result + (short_text != null ? short_text.hashCode() : 0);
		result = 31 * result + (full_text != null ? full_text.hashCode() : 0);
		result = 31 * result + (creation_date != null ? creation_date.hashCode() : 0);
		result = 31 * result + (modification_date != null ? modification_date.hashCode() : 0);
		return result;
	}
}
