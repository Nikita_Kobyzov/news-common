package by.epam.newsmanagement.dao;

import by.epam.newsmanagement.dao.exception.DaoException;
import by.epam.newsmanagement.domain.Author;

/**
 * <p>
 * This interface describes the basic methods for DAO that work with
 * Author-entity
 * </p>
 *
 * @author Nikita Kobyzov
 * @see IGenericDao
 * @see by.epam.newsmanagement.dao.oracle.AuthorDaoImpl
 */
public interface IAuthorDao extends IGenericDao<Long, Author> {

    /**
     * <p>This method reads the author on a news id</p>
     *
     * @param news_id is a unique identifier of the news
     * @return the user corresponding to the given news id or <tt>null</tt> if
     * there isn't author with the appropriate news id
     * @throws DaoException if the error occurred in working with the data source
     */
    Author readOnNewsId(long news_id) throws DaoException;

    /**
     * <p>
     * This method makes author expired. In field "expired" sets the current
     * date
     * </p>
     *
     * @param author_id is a unique identifier of the author that to be expired
     * @throws DaoException if the error occurred in working with the data source
     */
    void makeExpired(long author_id) throws DaoException;
}
