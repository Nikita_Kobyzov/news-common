package by.epam.newsmanagement.dao.oracle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import by.epam.newsmanagement.dao.INewsDao;
import by.epam.newsmanagement.dao.exception.DaoException;
import by.epam.newsmanagement.dao.fieldhelper.NewsField;
import by.epam.newsmanagement.dao.searchcriteria.SearchCriteria;
import by.epam.newsmanagement.dao.sql.SqlRequest;
import by.epam.newsmanagement.domain.Author;
import by.epam.newsmanagement.domain.News;
import by.epam.newsmanagement.domain.Tag;

/**
 * <p>
 * This class works with the entity of the news from the Oracle database
 * </p>
 *
 * @author Mikita_Kobyzau
 */
@Component
public class NewsDaoImpl implements INewsDao {

    @Autowired
    private DataSource dataSource;

    @Override
    public Long create(News entity) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SqlRequest.SQL_CREATE_NEWS,
                new String[]{NewsField.NEWS_ID})) {
            ps.setString(1, entity.getTitle());
            ps.setString(2, entity.getShort_text());
            ps.setString(3, entity.getFull_text());
            ps.executeUpdate();
            try (ResultSet rs = ps.getGeneratedKeys()) {
                rs.next();
                entity.setNews_id(rs.getLong(1));
            } catch (SQLException e) {
                throw new DaoException("Exception in working with the database with entity = " + entity, e);
            }
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with entity = " + entity, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return entity.getNews_id();
    }

    @Override
    public News read(Long key) throws DaoException {
        News news;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SqlRequest.SQL_GET_NEWS_ON_ID)) {
            ps.setLong(1, key);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    news = this.getNewsFromResultSet(resultSet);
                } else {
                    return null;
                }
            } catch (SQLException e) {
                throw new DaoException("Exception in working with the database with key = " + key, e);
            }
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with key = " + key, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return news;
    }

    @Override
    public List<News> read() throws DaoException {
        List<News> newsList = new ArrayList<News>();
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (Statement statement = connection.createStatement();
             ResultSet rs = statement.executeQuery(SqlRequest.SQL_GET_ALL_NEWS)) {
            while (rs.next()) {
                newsList.add(this.getNewsFromResultSet(rs));
            }
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return newsList;
    }

    @Override
    public boolean update(Long key, News entity) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SqlRequest.SQL_UPDATE_NEWS)) {
            ps.setString(1, entity.getTitle());
            ps.setString(2, entity.getShort_text());
            ps.setString(3, entity.getFull_text());
            ps.setLong(4, key);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with key = "
                    + key + ", entity = " + entity, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public boolean delete(Long key) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SqlRequest.SQL_DELETE_NEWS)) {
            ps.setLong(1, key);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with key = " + key, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public List<News> readByCriteria(Long page, Integer newsPerPage, SearchCriteria criteria) throws DaoException {
        List<News> newsList = new ArrayList<News>();
        Connection connection = DataSourceUtils.getConnection(dataSource);
        String query = SqlRequest.SQL_NEWS_PAGINATION_LEFT_QUERY + getSearchCriteriaQuery(criteria)
                + SqlRequest.SQL_NEWS_PAGINATION_RIGHT_QUERY;
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            long first_news_position = newsPerPage * (page - 1) + 1;
            long last_news_position = page * newsPerPage;
            ps.setLong(1, last_news_position);
            ps.setLong(2, first_news_position);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    newsList.add(this.getNewsFromResultSet(rs));
                }
            } catch (SQLException e) {
                throw new DaoException("Exception in working with the database with page = "
                        + page + ", newsPerPage = " + newsPerPage + ", criteria = " + criteria, e);
            }
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with page = "
                    + page + ", newsPerPage = " + newsPerPage + ", criteria = " + criteria, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return newsList;
    }

    @Override
    public boolean connectNewsWithTag(long news_id, long tag_id) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SqlRequest.SQL_CREATE_NEWS_TAG)) {
            ps.setLong(1, news_id);
            ps.setLong(2, tag_id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with news_id = "
                    + news_id + ", tag_id = " + tag_id, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public boolean connectNewsWithAuthor(long news_id, long author_id) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SqlRequest.SQL_CREATE_NEWS_AUTHOR)) {
            ps.setLong(1, news_id);
            ps.setLong(2, author_id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with news_id = "
                    + news_id + ", author_id = " + author_id, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public boolean deleteNewsTag(long news_id) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SqlRequest.SQL_DELETE_NEWS_TAG)) {
            ps.setLong(1, news_id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with news_id = " + news_id, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public boolean deleteNewsAuthor(long news_id) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SqlRequest.SQL_DELETE_NEWS_AUTHOR)) {
            ps.setLong(1, news_id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with news_id = " + news_id, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    /**
     * <p>
     * This method create a query from search criteria
     * </p>
     *
     * @param criteria is a object that contains the search criteria
     * @return a search query
     */
    private String getSearchCriteriaQuery(SearchCriteria criteria) {
        StringBuilder sb = new StringBuilder();
        sb.append(SqlRequest.SQL_GET_ALL_NEWS_ON_CRITERIA);
        boolean isTagAdded = false;
        if (criteria.getTagList() != null && !criteria.getTagList().isEmpty()) {
            isTagAdded = true;
            sb.append(SqlRequest.BASIC_QUERY_BY_TAGS);
            for (Long tag_id : criteria.getTagList()) {
                sb.append(tag_id);
                sb.append(SqlRequest.COMMA);
            }
            sb.deleteCharAt(sb.length() - 1);
            sb.append(SqlRequest.CLOSE_BRACKET);
        }
        if (criteria.getAuthorList() != null && !criteria.getAuthorList().isEmpty()) {
            if (isTagAdded) {
                sb.append(SqlRequest.AND_PHRASE + SqlRequest.OPEN_BRACKET);
            }
            for (Long author_id : criteria.getAuthorList()) {
                sb.append(SqlRequest.BASIC_QUERY_BY_AUTHORS + author_id);
                sb.append(SqlRequest.OR_PHRASE);
            }
            sb.delete(sb.length() - 3, sb.length() - 1);
            if (isTagAdded) {
                sb.append(SqlRequest.CLOSE_BRACKET);
            }
        }
        return sb.toString();
    }

    /**
     * <p>
     * This method reads one record from the {@link java.sql.ResultSet}
     * </p>
     *
     * @param rs is an resulting query
     * @return an object of class {@link by.epam.newsmanagement.domain.News}
     * @throws SQLException if the error occurred in working with the database
     */
    private News getNewsFromResultSet(ResultSet rs) throws SQLException {
        News news = new News();
        news.setNews_id(rs.getLong(NewsField.NEWS_ID));
        news.setTitle(rs.getString(NewsField.TITLE));
        news.setShort_text(rs.getString(NewsField.SHOTR_TEXT));
        news.setFull_text(rs.getString(NewsField.FULL_TEXT));
        news.setCreation_date(rs.getTimestamp(NewsField.CREATION_DATE));
        news.setModification_date(rs.getTimestamp(NewsField.MODIFICATION_DATE));
        return news;
    }

}
