package by.epam.newsmanagement.dao.oracle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import by.epam.newsmanagement.dao.IUserDao;
import by.epam.newsmanagement.dao.exception.DaoException;
import by.epam.newsmanagement.dao.fieldhelper.UserField;
import by.epam.newsmanagement.dao.sql.SqlRequest;
import by.epam.newsmanagement.domain.User;

/**
 * <p>
 * This class works with the entity of the user from the Oracle database
 * </p>
 *
 * @author Mikita_Kobyzau
 */
@Component
public class UserDaoImpl implements IUserDao {

    @Autowired
    private DataSource dataSource;

    @Override
    public Long create(User entity) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SqlRequest.SQL_CREATE_USER,
                new String[]{UserField.USER_ID})) {
            ps.setString(1, entity.getUser_name());
            ps.setString(2, entity.getLogin());
            ps.setString(3, entity.getPassword());
            ps.executeUpdate();
            try (ResultSet rs = ps.getGeneratedKeys()) {
                rs.next();
                entity.setUser_ID(rs.getLong(1));
            } catch (SQLException e) {
                throw new DaoException("Exception in working with the database with entity = " + entity, e);
            }
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with entity = " + entity, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return entity.getUser_ID();

    }

    @Override
    public User read(Long key) throws DaoException {
        User user;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SqlRequest.SQL_GET_USER_ON_ID)) {
            ps.setLong(1, key);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    user = this.getUserFromResultSet(resultSet);
                } else {
                    return null;
                }
            } catch (SQLException e) {
                throw new DaoException("Exception in working with the database with key = " + key, e);
            }
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with key = " + key, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return user;
    }

    @Override
    public List<User> read() throws DaoException {
        List<User> userList = new ArrayList<User>();
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (Statement statement = connection.createStatement();
             ResultSet rs = statement.executeQuery(SqlRequest.SQL_GET_ALL_USERS)) {
            while (rs.next()) {
                userList.add(this.getUserFromResultSet(rs));
            }
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return userList;
    }

    @Override
    public boolean update(Long key, User entity) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SqlRequest.SQL_UPDATE_USER)) {
            ps.setString(1, entity.getUser_name());
            ps.setString(2, entity.getLogin());
            ps.setString(3, entity.getPassword());
            ps.setLong(4, key);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with key = "
                    + key + ", entity = " + entity, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public boolean delete(Long key) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SqlRequest.SQL_DELETE_USER)) {
            ps.setLong(1, key);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with key = " + key, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public User readOnLogin(String login) throws DaoException {
        User user;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SqlRequest.SQL_GET_USER_ON_LOGIN)) {
            ps.setString(1, login);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    user = this.getUserFromResultSet(resultSet);
                } else {
                    return null;
                }
            } catch (SQLException e) {
                throw new DaoException("Exception in working with the database with login = " + login, e);
            }
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with login = " + login, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return user;
    }

    /**
     * <p>
     * This method reads one record from the {@link java.sql.ResultSet}
     * </p>
     *
     * @param rs is an resulting query
     * @return an object of class {@link by.epam.newsmanagement.domain.User}
     * @throws SQLException if the error occurred in working with the database
     */
    private User getUserFromResultSet(ResultSet rs) throws SQLException {
        User user = new User();
        user.setUser_ID(rs.getLong(UserField.USER_ID));
        user.setUser_name(rs.getString(UserField.USER_NAME));
        user.setLogin(rs.getString(UserField.LOGIN));
        user.setPassword(rs.getString(UserField.PASSWORD));
        return user;
    }
}
