package by.epam.newsmanagement.dao.oracle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import by.epam.newsmanagement.dao.IRoleDao;
import by.epam.newsmanagement.dao.exception.DaoException;
import by.epam.newsmanagement.dao.fieldhelper.RoleField;
import by.epam.newsmanagement.dao.sql.SqlRequest;
import by.epam.newsmanagement.domain.Role;

/**
 * <p>
 * This class works with the entity of the role from the Oracle database
 * </p>
 *
 * @author Mikita_Kobyzau
 */
@Component
public class RoleDaoImpl implements IRoleDao {

    @Autowired
    private DataSource dataSource;

    @Override
    public Long create(Role entity) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SqlRequest.SQL_CREATE_ROLE,
                new String[]{RoleField.ROLE_ID})) {
            ps.setLong(1, entity.getUser_id());
            ps.setString(2, entity.getRole_name());
            ps.executeUpdate();
            try (ResultSet rs = ps.getGeneratedKeys()) {
                rs.next();
                entity.setRole_id(rs.getLong(1));
            } catch (SQLException e) {
                throw new DaoException("Exception in working with the database with entity = " + entity, e);
            }
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with entity = " + entity, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return entity.getRole_id();
    }

    @Override
    public Role read(Long key) throws DaoException {
        Role role;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SqlRequest.SQL_GET_ROLE_ON_ID)) {
            ps.setLong(1, key);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    role = this.getRoleFromResultSet(resultSet);
                } else {
                    return null;
                }
            } catch (SQLException e) {
                throw new DaoException("Exception in working with the database with key = " + key, e);
            }
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with key = " + key, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return role;
    }

    @Override
    public List<Role> read() throws DaoException {
        List<Role> roleList = new ArrayList<Role>();
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (Statement statement = connection.createStatement();
             ResultSet rs = statement.executeQuery(SqlRequest.SQL_GET_ALL_ROLES)) {
            while (rs.next()) {
                roleList.add(this.getRoleFromResultSet(rs));
            }
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return roleList;
    }

    @Override
    public boolean update(Long key, Role entity) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SqlRequest.SQL_UPDATE_ROLE)) {
            ps.setString(1, entity.getRole_name());
            ps.setLong(2, key);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with key = "
                    + key + ", entity = " + entity, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public boolean delete(Long key) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SqlRequest.SQL_DELETE_ROLE)) {
            ps.setLong(1, key);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with key = " + key, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    /**
     * <p>
     * This method reads one record from the {@link java.sql.ResultSet}
     * </p>
     *
     * @param rs is an resulting query
     * @return an object of class {@link by.epam.newsmanagement.domain.Role}
     * @throws SQLException if the error occurred in working with the database
     */
    private Role getRoleFromResultSet(ResultSet rs) throws SQLException {
        Role role = new Role();
        role.setRole_id(rs.getLong(RoleField.ROLE_ID));
        role.setRole_name(rs.getString(RoleField.ROLE_NAME));
        role.setUser_id(rs.getLong(RoleField.USER_ID));
        return role;
    }

}
