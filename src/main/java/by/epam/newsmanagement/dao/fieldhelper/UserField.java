package by.epam.newsmanagement.dao.fieldhelper;

/**
 * <p>
 * This class contains the names of the user field
 * </p>
 * 
 * @author Mikita_Kobyzau
 *
 */
public final class UserField {
	public static final String USER_ID = "USER_ID";
	public static final String USER_NAME = "USER_NAME";
	public static final String LOGIN = "USER_LOGIN";
	public static final String PASSWORD = "USER_PASSWORD";
}
